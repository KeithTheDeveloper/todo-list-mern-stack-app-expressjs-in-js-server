const asyncHandler = require("express-async-handler");
const Todo = require("../models/todoModel");

/**
 * @desc Get Todos
 * @param req
 * @param res
 * @route GET /todos
 * @access Private
 */
const getTodos = asyncHandler(async (req, res) => {
  const todos = await Todo.find();
  res.status(200).json(todos);
});

/**
 * @desc Set Todo
 * @param req
 * @param res
 * @route PUT /todos
 * @access Private
 */
const setTodo = asyncHandler(async (req, res) => {
  if (!req.body.title) {
    res.status(400);
    throw new Error("Please add A Title field");
  }
  const newTodo = Todo.create({
    title: req.body.title,
    desc: req.body.desc,
    isComplete: req.body.isComplete,
  });
  res.status(200).json(newTodo);
});

//Update Todo
const updateTodo = asyncHandler(async (req, res) => {
  const todo = await Todo.findById(req.params._id);
  if (!todo) {
    res.status(400);
    throw new Error(`Todo with id-${req.params._id}  not found`);
  }
  const updatedTodo = await Todo.findByIdAndUpdate(req.params._id, req.body, {
    new: true,
  });
  res.status(200).json(updatedTodo);
});

//Delete Todo
const deleteTodo = asyncHandler(async (req, res) => {
  const todoToDelete = await Todo.findById(req.params._id);
  if (!todoToDelete) {
    res.status(400);
    throw new Error(`Todo with id-${req.params._id}  not found`);
  }
  await todoToDelete.remove();
  res.status(200).json({ id: req.params._id });
});

module.exports = {
  deleteTodo,
  updateTodo,
  setTodo,
  getTodos,
};
