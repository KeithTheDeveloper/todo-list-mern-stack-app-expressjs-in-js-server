# Todo List MERN Stack App ExpressJS in JS Server

This is A ExpressJS Server Written in JS for a
Todo List MERN Stack App

## Authors

- [@KeithTheDeveloper](https://gitlab.com/KeithTheDeveloper)

## Badges

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Tech Stack

**Server:** Node, Express,JavaScript
