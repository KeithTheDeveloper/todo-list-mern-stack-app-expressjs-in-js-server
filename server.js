const express = require("express");
const colors = require("colors");
const dotenv = require("dotenv").config();

const todoRoutes = require("./routes/todoRoutes");
const userRoutes = require("./routes/userRoutes.js");
const { errorHandler } = require("./middleware/errorMiddleware");
const connectDB = require("./config/db");

connectDB();
// rest of the code remains same
const app = express();
const PORT = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/api/todos", todoRoutes);
app.use("/api/users", userRoutes);
app.use(errorHandler);
app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});
